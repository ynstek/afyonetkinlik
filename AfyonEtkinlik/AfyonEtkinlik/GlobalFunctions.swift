//
//  GlobalFunctions.swift
//  Orca POS Mobile
//
//  Created by Yunus TEK on 21.03.2017.
//  Copyright © 2017 Orca Businesss Solutions. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import FirebaseMessaging
import Firebase
import UserNotifications
import UserNotificationsUI

private let _sharedGlobalFunctions = GlobalFunctions()
class GlobalFunctions : NSObject, URLSessionDelegate, CLLocationManagerDelegate {
    
    // MARK: - SHARED INSTANCE
    class var shared : GlobalFunctions {
        return _sharedGlobalFunctions
    }
    
    func firebaseSetTokenAndTopic(_ tokenId: String? = nil) {
        // TODO: Firebase - Abone olma uygulama acildiktan sonra yapilmali. yoksa null veriyor ve abone olamiyor.
        // [START subscribe_topic]
        Messaging.messaging().subscribe(toTopic: "news")
        print("Subscribed to news topic")
        // [END subscribe_topic]
        
        // [START log_fcm_reg_token]
        var token = Messaging.messaging().fcmToken ?? ""
        print("FCM token: \(token)")
        // [END log_fcm_reg_token]
        
        if tokenId != nil {
            token = tokenId!
        }
        
        JsonToken.Todo.connect(tokenId: token)
    }
    class func getTopController() -> UIViewController {
        var topController = UIApplication.shared.keyWindow!.rootViewController! as UIViewController
        while ((topController.presentedViewController) != nil) {
            topController = topController.presentedViewController!;
        }
        return topController
    }
}


extension UIImageView {
    func df(_ url: URL, _ completion: @escaping () -> Void) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
                completion()
            }
            }.resume()
    }
    
    func downloadedFrom(link: String?, _ completion: @escaping () -> Void) {
        if link != nil {
            guard let url = URL(string: link!) else { return }
            df(url) {
                completion()
            }
        }
    }
}

extension UNNotificationAttachment {
    
    static func create(identifier: String, image: UIImage, options: [NSObject : AnyObject]?) -> UNNotificationAttachment? {
        let fileManager = FileManager.default
        let tmpSubFolderName = ProcessInfo.processInfo.globallyUniqueString
        let tmpSubFolderURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(tmpSubFolderName, isDirectory: true)
        do {
            try fileManager.createDirectory(at: tmpSubFolderURL, withIntermediateDirectories: true, attributes: nil)
            let imageFileIdentifier = identifier+".png"
            let fileURL = tmpSubFolderURL.appendingPathComponent(imageFileIdentifier)
            guard let imageData = UIImagePNGRepresentation(image) else {
                return nil
            }
            try imageData.write(to: fileURL)
            let imageAttachment = try UNNotificationAttachment.init(identifier: imageFileIdentifier, url: fileURL, options: options)
            return imageAttachment
        } catch {
            print("error " + error.localizedDescription)
        }
        return nil
    }
}
