//
//  ViewController.swift
//  AfyonEtkinlik
//
//  Created by Dark Matter on 13/04/2018.
//  Copyright © 2018 ghost. All rights reserved.
//

import UIKit
import WebKit
import MapKit

class ViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var isMapActive = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadWebView(_:)), name: NSNotification.Name(rawValue: "reloadWebView"), object: nil)
        
        webView.delegate = self
        webView.isHidden = true
        openWebView("http://etkinlik.tasarimedya.com")
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        GlobalFunctions.shared.firebaseSetTokenAndTopic()
//    }
    
    @objc func reloadWebView(_ notification: NSNotification) {
        if let urlString = AppDelegate.link {
            AppDelegate.link = nil
            openWebView(urlString)
        }
    }
    
    func openWebView(_ urlString: String) {
        let url = URL(string: urlString)
        let request = URLRequest(url: url!)
        
        webView.loadRequest(request)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        //        if (navigationType == UIWebViewNavigationType.formSubmitted) {
        print("SUBMIT", request.url!.absoluteString)
        
        if ((request.url?.absoluteString.range(of: "error_code=200")) != nil) {
            openWebView("http://etkinlik.tasarimedya.com/uye")
        }
        
        GlobalFunctions.shared.firebaseSetTokenAndTopic()
        let controlHtml: String = webView.stringByEvaluatingJavaScript(from: "document.documentElement.outerHTML.toString()")!
        
        if ((request.url?.absoluteString.range(of:
            "https://www.google.com/maps/dir/?api=1&origin=&destination=")) != nil) {
            var lat = ""
            var long = ""
            
            var start = "&destination="
            var end = ","
            if let match = controlHtml.range(of: "(?<=\(start))[^\(end)]+", options: .regularExpression) {
                lat = String(controlHtml[match])
            }
            
            start = "&destination=\(lat),"
            end = "\\';\n"
            if let match = controlHtml.range(of: "(?<=\(start))[^\(end)]+", options: .regularExpression) {
                long = String(controlHtml[match])
            }
            
            self.yolTarifiAl(lat: lat, long: long)
        }
        
//        if request.url?.absoluteString == "http://etkinlik.tasarimedya.com/uye/ayarlar" {
//            GlobalFunctions.shared.firebaseSetTokenAndTopic()
//        } else if request.url?.absoluteString == "http://etkinlik.tasarimedya.com/uye" {
//            GlobalFunctions.shared.firebaseSetTokenAndTopic()
//        } else if request.url?.absoluteString == "http://etkinlik.tasarimedya.com" {
//            GlobalFunctions.shared.firebaseSetTokenAndTopic()
//        } else if request.url?.absoluteString == "http://etkinlik.tasarimedya.com/haberler" {
//            GlobalFunctions.shared.firebaseSetTokenAndTopic()
//        }
        
        //        }
        
        webView.isHidden = false
        return true
    }
    
    func yolTarifiAl(lat: String, long: String) {
        if isMapActive == false {
            self.isMapActive = true
            let requestCLLocation = CLLocation(latitude: Double(lat)!, longitude: Double(long)!)
            CLGeocoder().reverseGeocodeLocation(requestCLLocation) { (placemarks, error) in
                if let placemark = placemarks {
                    if placemark.count > 0 {
                        let newPlacemark = MKPlacemark(placemark: placemark[0])
                        let item = MKMapItem(placemark: newPlacemark)
                        item.name = ""
                        
                        let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
                        item.openInMaps(launchOptions: launchOptions)
                    }
                    self.webView.goBack()
                    self.isMapActive = false
                }
            }
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.isHidden = false
        activityIndicator.stopAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    override var prefersStatusBarHidden: Bool {
//        return true
//    }

}




