//
//  Reachability.swift
//  Orca POS Mobile
//
//  Created by Yunus TEK on 22.05.2017.
//  Copyright © 2017 Orca Businesss Solutions. All rights reserved.
//

import Foundation
import SystemConfiguration
import UIKit

// Network Connection Control
open class Reachability {
    
    class func isConnectedToNetwork(_ error: Bool = true) -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        let status = (isReachable && !needsConnection)

        if !status && error {
            openNoInternetView()
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "İnternet bağlantınız çevrimdışı görünüyor. Bir ağa bağlanıp tekrar deneyiniz.")
        }
        
        return status
    }
    
    class fileprivate func openNoInternetView() {
        let popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "noInternetID")
        
        popover.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        popover.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        let popoverCont = popover.popoverPresentationController
        popoverCont?.permittedArrowDirections = .any
        //        popoverCont?.sourceView = sender as? UIView
        GlobalFunctions.getTopController().present(popover, animated: true, completion: nil)
    }
}

