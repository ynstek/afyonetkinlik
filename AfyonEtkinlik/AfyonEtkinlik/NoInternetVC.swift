//
//  NoInternetVC.swift
//  AfyonEtkinlik
//
//  Created by Yunus Tek on 21.04.2018.
//  Copyright © 2018 ghost. All rights reserved.
//

import UIKit

class NoInternetVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBaglan() {
        if Reachability.isConnectedToNetwork(false) {
            openHomeView()
        }
    }
    
    fileprivate func openHomeView() {
        let popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeID")
        
        popover.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        popover.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        let popoverCont = popover.popoverPresentationController
        popoverCont?.permittedArrowDirections = .any
        //        popoverCont?.sourceView = sender as? UIView
        GlobalFunctions.getTopController().present(popover, animated: true, completion: nil)
    }

}
