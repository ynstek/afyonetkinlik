
//
//  JsonCity.swift
//  TonyCode
//
//  Created by Yunus Tek on 17.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation
import UIKit

class JsonToken : NSObject  {
    
    static func endpointForTodos(_ tokenId: String) -> String {
        let deviceId = (UIDevice.current.identifierForVendor?.uuidString)!
        return "http://etkinlik.tasarimedya.com/ios/device?devicetoken_number=\(tokenId)&device_id=\(deviceId)"
    }
    
    struct Todo: Codable {
        static func connect(tokenId: String) {
            if Reachability.isConnectedToNetwork() {
                let endpoint = endpointForTodos(tokenId)
                guard let url = URL(string: endpoint) else {
                    print("Error: cannot create URL")
                    //                AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Could not construct URL")
                    return
                }
                let urlRequest = URLRequest(url: url)
                
                let session = URLSession.shared
                
                let task = session.dataTask(with: urlRequest) {(data, response, error) in
                    guard data != nil else {
                        print("Error: did not receive data", error!)
                        //                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Did not receive data\n" + error!.localizedDescription)
                        return
                    }
                    
                    guard error == nil else {
                        //                    AlertFunctions.mess/ageType.showOKAlert("Error", bodyMessage: error!.localizedDescription)
                        return
                    }
                    
                }
                task.resume()
            }
        }
        
    }
}

