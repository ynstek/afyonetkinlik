//
//  AppDelegate.swift
//  AfyonEtkinlik
//
//  Created by Dark Matter on 13/04/2018.
//  Copyright © 2018 ghost. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications
import UserNotificationsUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id" // firebase
    static var link: String? = nil
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.createFirebase(application)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}

extension AppDelegate : UNUserNotificationCenterDelegate, MessagingDelegate {
    
    // MARK: Firebase
    func createFirebase(_ application: UIApplication) {
        
        // register_for_notifications
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound ]
            
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications() // if else ten sonra olmali.
        
        // MARK: Firebase Configure
        FirebaseApp.configure()
        
        let current = UNUserNotificationCenter.current()
        current.delegate = self
        
        // Uygulama bildirim sayisini sifirlama
        application.applicationIconBadgeNumber = 0
        current.removeAllDeliveredNotifications() // To remove all delivered notifications
        current.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self //as? MessagingDelegate
        // [END set_messaging_delegate]
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed. Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // uygulama acikken bildirimi yakalar
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (_ options: UNNotificationPresentationOptions) -> Void) {
        
        if let link = notification.request.content.userInfo["link"] as? String {
            print("link", link)
            AppDelegate.link = link
        }
        
        completionHandler([.badge, .alert, .sound])
    }
    
    // MARK: [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
         Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        application.applicationIconBadgeNumber += 1
        print(userInfo)
    }
    
    // MARK: Bildirime tiklaninca
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        if let link = userInfo["link"] as? String {
            print("link", link)
            AppDelegate.link = link
            NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadWebView"), object: nil)
        }
        
        if let resim = userInfo["resim"] as? String {
            print("resim", resim)
        }
        
        application.applicationIconBadgeNumber -= 1
        Messaging.messaging().appDidReceiveMessage(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        GlobalFunctions.shared.firebaseSetTokenAndTopic(fcmToken)
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    // TEST
    func withImage(_ userInfo: [AnyHashable : Any]) {
        let content = UNMutableNotificationContent()
        content.title = ""
        content.subtitle = ""
        content.body = ""
        content.sound = UNNotificationSound.default()
        content.badge = (UIApplication.shared.applicationIconBadgeNumber + 1) as NSNumber;
        content.categoryIdentifier = "notification-category"
        //        content.sound = UNNotificationSound.init(named:"golsesi.aiff")
        //content.threadIdentifier = "notification-thread"
        
        let current = UNUserNotificationCenter.current()
        current.delegate = self
        
        let imageV = UIImageView()
        let urlString = userInfo["resim"] as? String
        imageV.downloadedFrom(link: urlString) {
            if let attachment = UNNotificationAttachment.create(identifier: "image", image: imageV.image!, options: nil) {
                // where myImage is any UIImage that follows the
                content.attachments = [attachment]
            }
            // Deliver the notification in five seconds.
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval:5, repeats: false)
            
            // Schedule the notification.
            let request = UNNotificationRequest.init(identifier: "id-123", content: content, trigger: trigger)
            let center = UNUserNotificationCenter.current()
            center.add(request)
        }
    }
}


