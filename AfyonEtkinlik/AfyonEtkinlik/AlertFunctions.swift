//
//  AlertFunctions.swift
//  Orca POS Mobile
//
//  Created by Yunus TEK on 21.03.2017.
//  Copyright © 2017 Orca Businesss Solutions. All rights reserved.
//

import UIKit
import Foundation

private let _sharedAlertFunctions = AlertFunctions()

class AlertFunctions : NSObject  {
    
    // MARK: - SHARED INSTANCE
    class var messageType : AlertFunctions {
        return _sharedAlertFunctions
    }
    
    func showYesNoAlert(_ titleMessage: String, bodyMessage: String, _ yes: @escaping () -> Void, no: @escaping () -> Void){
        
        let alertController = UIAlertController(title: titleMessage, message: bodyMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Evet", style: .default , handler: { (action: UIAlertAction!) in
            yes()
            //            return true
            print("Yes")
        }))
        
        alertController.addAction(UIAlertAction(title: "Hayır", style: .cancel , handler: {(action: UIAlertAction!) in
            no()
            //            return true
            print("No")
        }))
        
        GlobalFunctions.getTopController().present(alertController, animated:true, completion:nil)
    }
    
    func showOKAlert(_ titleMessage: String, bodyMessage: String) {
        
        let alertController = UIAlertController(title: titleMessage, message: bodyMessage, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "Tamam", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        GlobalFunctions.getTopController().present(alertController, animated:true, completion:nil)
    }
    
    func showOKAlertVoid(_ titleMessage: String, bodyMessage: String, _ tamam: @escaping () -> Void){
        
        let alertController = UIAlertController(title: titleMessage, message: bodyMessage, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Tamam", style: .default , handler: { (action: UIAlertAction!) in
            tamam()
        }))
        
        GlobalFunctions.getTopController().present(alertController, animated:true, completion:nil)
    }
    
    func showInfoAlert(_ titleMessage: String, bodyMessage: String){
        
        let alertController = UIAlertController(title: titleMessage, message: bodyMessage, preferredStyle: .alert)
        GlobalFunctions.getTopController().present(alertController, animated:true, completion:nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            alertController.dismiss(animated: false, completion: nil)
        }
    }
    
    func showYesNoAlertWithSwitch(_ titleMessage: String, bodyMessage: String, _ switchTitle: String, _ switchStatus: Bool, success: @escaping (_ status: Bool) -> Void) {
        // private Alert
        let alert = UIAlertController(title: titleMessage, message: bodyMessage + "\n\n", preferredStyle: .alert)
        
        // Create Label
        let labelFrame: CGRect = CGRect(x: 65, y: 75, width: 150, height: 20)
        let label: UILabel! = UILabel(frame: labelFrame)
        label.text = switchTitle
        alert.view.addSubview(label)
        
        // Create Switch Object
        let switchFrame: CGRect = CGRect(x: 160, y: 70, width: 20, height: 20)
        let switchObject: UISwitch! = UISwitch(frame: switchFrame)
        switchObject.isOn = switchStatus
        
        alert.view.addSubview(switchObject)
        
        alert.addAction(UIAlertAction(title: "Evet", style: .default , handler: { (alert) in
            print("Yes", "Show Price:", switchObject.isOn)
            success(switchObject.isOn)
        }))
        
        alert.addAction(UIAlertAction(title: "Hayır", style: .cancel , handler: {(alert) in
            print("No")
        }))
        
        GlobalFunctions.getTopController().present(alert, animated: true, completion: nil)
    }

}


